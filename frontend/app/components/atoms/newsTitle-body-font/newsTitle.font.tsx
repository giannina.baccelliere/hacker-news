import { Barlow_Condensed } from "@next/font/google";

const title2Font = Barlow_Condensed({
  weight: "200",
  style: ["normal"],
  subsets: ["latin"],
});

export default title2Font;
