import { Aboreto } from "@next/font/google";

const title1Font = Aboreto({
  weight: "400",
  style: ["normal"],
  subsets: ["latin"],
});

export default title1Font;
